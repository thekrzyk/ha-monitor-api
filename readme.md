# Home Assistant - Monitoring API

**Note: This is a non-dockerized mod of the original https://github.com/ned-kelly/ha-monitor-api**

Welcome to the lightweight "ha-monitor-api" project! 

This is a quick-and-dirty lightweight API _(knocked out in an evening)_ designed to expose the system's current metrics _(such as disk, network, cpu/temperature etc)_ as a simple JSON endpoint that your Home Assistant instance can query.


## Configuration Variables

By default the service listens on TCP:9999 (on all interfaces). If you need to change the port, you may do so by setting the `LISTEN_PORT` environment variable in your compose file.

## Installing and running on rPI3 with PM2

Install NodeJS:
```
curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt install nodejs
```

Install PM2:
```
sudo npm install pm2 -g
pm2 startup
# PM2 will tell you to run some command, eg.:
sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u pi --hp /home/pi
```

Get the code:
```
git clone https://git.krzyk.eu.org/thywolf/ha-monitor-api
cd ./ha-monitor-api/src
npm install
```

Demonize and run:
```
pm2 start server.js --name "ha-monitor-api" --watch
pm2 save
```

## Integrating to Home Assistant

Integrating into Home Assistant can be done like any other sensor using the [RESTful Sensor](https://www.home-assistant.io/components/sensor.rest/).

An example here is taken from my OrangePI - _(Running [Facial Recognition](https://www.hackster.io/97766/announce-who-is-home-using-facial-recognition-dcc389) by scraping all my friends's profile pictures off Facebook)..._

**_Your (sensors/configuration.yaml) file:_**

```yaml
# Sensor to monitor system resources for the Front Door PI.
- platform: rest
  name: Front Door
  resource: http://10.16.10.144:9999/
  timeout: 30
  json_attributes:
    - cpu_current
    - cpu_free
    - cpu_average
    - cpu_temperature
    - drive
    - memory
    - network
  value_template: '{{ value_json.value}}'
  headers:
    Content-Type: application/json
    User-Agent: Home Assistant Agent

# To use the data on the Home Assistant Lovelace Dashboard we need to extract the values from the sensor, and store them as their own sensor values...
- platform: template
  sensors:
    front_door_cputemp:
      value_template: '{{ states.sensor.front_door.attributes["cpu_temperature"] }}'
      unit_of_measurement: '°'
      entity_id: sensor.front_door
    front_door_cpuused:
      value_template: '{{ states.sensor.front_door.attributes["cpu_current"] }}'
      unit_of_measurement: '%'
      entity_id: sensor.front_door
    front_door_freegb:
      value_template: '{{ states.sensor.front_door.attributes["drive"]["freeGb"] }}'
      unit_of_measurement: 'GB'
      entity_id: sensor.front_door
    front_door_driveused:
      value_template: '{{ states.sensor.front_door.attributes["drive"]["usedPercentage"] }}'
      unit_of_measurement: '%'
      entity_id: sensor.front_door
    front_door_freemem:
      value_template: '{{ states.sensor.front_door.attributes["memory"]["freeMemPercentage"] }}'
      unit_of_measurement: '%'
      entity_id: sensor.front_door
    front_door_networklevel:
      value_template: '{{ states.sensor.front_door.attributes["network"]["wlan0"]["wireless"]["qualityLevel"] }}'
      entity_id: sensor.front_door
    front_door_networklink:
      value_template: '{{ states.sensor.front_door.attributes["network"]["wlan0"]["wireless"]["qualityLink"] }}'
      entity_id: sensor.front_door
    front_door_networkin:
      value_template: '{{ states.sensor.front_door.attributes["network"]["wlan0"]["inputBytes"] | multiply(0.000000001024) | round(2) }}'
      entity_id: sensor.front_door
      unit_of_measurement: 'GB'
    front_door_networkout:
      # Multiply pipe will convert bytes to gb.
      value_template: '{{ states.sensor.front_door.attributes["network"]["wlan0"]["outputBytes"] | multiply(0.000000001024) | round(2) }}'
      entity_id: sensor.front_door
      unit_of_measurement: 'GB'

```

And of course, this data is all well and good - but what about putting it in the Lovelace UI? - I'm using the awesome ["vertical-stack-in-card"](https://github.com/custom-cards/vertical-stack-in-card) here to combine multiple cards as one card like so:

![Example Screenshot](images/lovelace-example.png "Example Screenshot")

**_Your Lovelace raw YAML config:_**

```yaml
show_header_toggle: false
title: Front Door
type: 'custom:vertical-stack-in-card'
cards:
  - type: glance
    columns: 4
    entities:
      - entity: sensor.front_door_networkin
        icon: 'mdi:download'
        name: Download (Gb)
      - entity: sensor.front_door_networkout
        icon: 'mdi:upload'
        name: Upload (Gb)
      - entity: sensor.front_door_networklevel
        icon: 'mdi:radio-tower'
        name: Signal
      - entity: sensor.front_door_networklink
        icon: 'mdi:speedometer'
        name: Link
  - type: history-graph
    entities:
      - entity: sensor.front_door_cpuused
        name: Core/CPU 1 Used %
  - type: glance
    columns: 3
    entities:
      - entity: sensor.front_door_cputemp
        icon: 'mdi:thermometer-lines'
        name: CPU Temperature (°C)
      - entity: sensor.front_door_freegb
        icon: 'mdi:harddisk'
        name: Disk Free (Gb)
      - entity: sensor.front_door_freemem
        icon: 'mdi:memory'
        name: Memory Free (%)
```

